package com.project.samirelidrissi;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements MuseCeri.ClickedItem{

    /*
    * DECLARATION DES ELEMENTS DANS LE FICHIER XML ( ACTIVITY_MAIN.XML)
    * */
    Toolbar toolbar;
    RecyclerView listDataView;
    MuseCeri museCeri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // INSTENTIATION
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        listDataView = findViewById(R.id.listdata);

        listDataView.setLayoutManager(new LinearLayoutManager(this));
        listDataView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));

        museCeri = new MuseCeri(this::ClickedMuseCeri);

        // CHARGEMENT DES DONNEES DEPUIS LE SERVEUR API
        getMuseCERI();
    }


    public void getMuseCERI(){
        //COMMUNICATION AVEC LE SERVICE
        ApiClient.getMuseService().getCollection().enqueue(
                new Callback<Map<String, MuseCeriResponse>>() {
                    @Override
                    public void onResponse(Call<Map<String, MuseCeriResponse>> call,
                                           Response<Map<String, MuseCeriResponse>> response) {

                        // DECLARATION DE LA LISTE DE COLLECTION CERIMUSUME
                        ArrayList<MuseCeriResponse> museListe=new ArrayList<>();
                        for (String key: response.body().keySet()) {
                            //REMPLIRE LA LISTE
                            museListe.add(new MuseCeriResponse(response.body().get(key),key));
                        }

                        //RENDU DE LA LISTE CHEZ L'UTILISATEUR
                        museCeri.setData(museListe);
                        listDataView.setAdapter(museCeri);
                    }

                    @Override
                    public void onFailure(Call<Map<String, MuseCeriResponse>> call, Throwable t) {
                        // ON CAS D'ERREUR !
                        Log.d("FAILURE API",t.getMessage());
                    }
                });
    }

    @Override
    public void ClickedMuseCeri(MuseCeriResponse museCeriResponse) {
        // ACCEE AU DETAIL APRES UN CLIQUE
        // ON PASSE LES DONNEE DE L'OBJET CLIQUER EN PARAMETTRE ( data )
        startActivity(new Intent(this, MuseCeriDetailsActivity.class).putExtra("data", museCeriResponse));
    }
}

