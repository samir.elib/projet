package com.project.samirelidrissi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

public class MuseCeri extends RecyclerView.Adapter<MuseCeri.MuseCeriVH> {

    // C'EST L'ADAPTEUR DU RENDU DE LA COLLECTION
    // IL PERMET D'AFFICHER CARD PAR CARD ( LIGNE )

    private List<MuseCeriResponse> museCeriResponseList;
    private Context context;
    private ClickedItem clickedItem;

    public MuseCeri(ClickedItem clickedItem) {
        this.clickedItem = clickedItem;
    }

    //RECEPTION DE LA LISTE DEPUIS LE MAIN UNE FOIS LA LISTE EST REMPLIE( voir la ligne 63 dans le fichier MainActivity)
    public void setData(List<MuseCeriResponse> museCeriResponseList) {
        this.museCeriResponseList = museCeriResponseList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MuseCeriVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        //SWITCHER LE STYLE D'AFFICHAGE ( DROITE / GAUCHE ) ( Rocky )
        if(viewType==1)
            return new MuseCeriVH(LayoutInflater.from(context).inflate(R.layout.card_layout,parent,false));
        else
            return new MuseCeriVH(LayoutInflater.from(context).inflate(R.layout.card_layout2,parent,false));
    }


    @Override
    public void onBindViewHolder(@NonNull MuseCeriVH holder, int position) {
        // RECUPERATION DE L'ELEMETN ON COURS
        MuseCeriResponse museCeriResponse = museCeriResponseList.get(position);
        /*
        try {

            InputStream srt = new URL(museCeriResponse.imageObject().toString()).openStream();
            Bitmap bitmap = BitmapFactory.decodeStream(srt);
            holder.imageObjet.setImageBitmap(bitmap);

        } catch (IOException e) {
            e.printStackTrace();
        }*/

        // REMPLIRE LES VARIABLES POUR LES AFFECTERS APRES
        String name = museCeriResponse.getName();
        String categorie = museCeriResponse.getCategories();

        //AFFICHER L'IMAGE
        Glide.with(holder.itemView)
                .load(museCeriResponse.imageObject())
                .error(R.drawable.ic_baseline_broken_image_24)
                .fallback(R.drawable.ic_baseline_broken_image_24)
                .into(holder.imageObject); //8

        holder.name.setText(name);
        holder.categorie.setText(categorie);
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //DECLANCHER LE CLICK VERS LE MAINACTIVITY
                clickedItem.ClickedMuseCeri(museCeriResponse);
            }
        });
    }

    public interface ClickedItem{
        public void ClickedMuseCeri(MuseCeriResponse museCeriResponse);
    }

    @Override
    public int getItemViewType(int position) {
        return position%2;
    } // PAIR OU IMPAIRE ( DROITE / GAUCHE )

    @Override
    public int getItemCount() {
        return museCeriResponseList.size();
    }

    // CLASS DE RENDU CARD POUR MANIPULARION
    public class MuseCeriVH extends RecyclerView.ViewHolder {

        TextView name;
        ImageView imageObject;
        TextView categorie;

        public MuseCeriVH(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            categorie = itemView.findViewById(R.id.categorie);
            imageObject = itemView.findViewById(R.id.imageObject);
        }
    }

}
