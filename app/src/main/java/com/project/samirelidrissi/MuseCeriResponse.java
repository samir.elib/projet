package com.project.samirelidrissi;

import android.net.Uri;

import android.text.TextUtils;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

// CLASS MAPPING ENTRE API ET ANDROID
public class MuseCeriResponse implements Serializable {

    private String id;
    private String name;
    private String description;
    private String[] categories;//Array
    private String[] timeFrame;//Array
    private String year="";
    private String brand="";
    private String[] technicalDetails;//Array
    private Map<String,String> pictures=null;//Array
    private boolean working;

    MuseCeriResponse(){
    }

    MuseCeriResponse(MuseCeriResponse body,String Key){
        id = Key;
        name = body.name;
        description = body.description;
        working = body.working;
        categories = body.categories;
        timeFrame = body.timeFrame;
        if(body.pictures!=null){
            pictures = body.pictures;
        }

        if (body.year!=null){
            year = body.year;
        }
        if (body.brand!=null){
           brand = body.brand;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getCategories() {
        return TextUtils.join(", ",categories);
    }

    public void setCategories(String[] categories) {
        this.categories = categories;
    }

    public String getTimeFrame() {
        return TextUtils.join(", ",timeFrame);
    }

    public void setTimeFrame(String[] timeFrame) {
        this.timeFrame = timeFrame;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String[] getTechnicalDetails() {
        return technicalDetails;
    }

    public void setTechnicalDetails(String[] technicalDetails) {
        this.technicalDetails = technicalDetails;
    }

    public List<String> getPictures() {
        if(pictures==null)
            return null;

        List<String> data = new ArrayList<String>();
        for(String key: pictures.keySet()){
            data.add("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+id+"/images/"+key);
        }
        return data;
    }

    public void setPictures(Map<String,String> pictures) {
        if(pictures!=null)
        this.pictures = pictures;
    }

    public Uri imageObject(){
        return Uri.parse("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+id+"/thumbnail");
    }

    public boolean isWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }

    /*
    @Override
    public String toString() {
        return "MuseCeriResponse{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", username='" + username + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", email='" + email + '\'' +
                ", is_active=" + is_active +
                ", date_joined='" + date_joined + '\'' +
                '}';
    }
    */
}
