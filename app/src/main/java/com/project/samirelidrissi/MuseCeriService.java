package com.project.samirelidrissi;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface MuseCeriService {

    @Headers({
        "Accept: json",
    })

    //APPEL A LA LISTE DE COLLECTION
    @GET("collection/")
    public Call<Map<String, MuseCeriResponse>> getCollection();

    /*
    // APPEL A L'IMAGE PAR ID ( KEY )
    @GET("items/{itemID}/thumbnail")
    Call<ResponseBody> retrieveImageData(@Path("itemID") String imageName);

     */
}
