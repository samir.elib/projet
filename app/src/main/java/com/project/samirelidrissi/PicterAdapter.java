package com.project.samirelidrissi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import java.util.List;

// GESTIONNAIRE D'AFFICHAGE DES IMAGES DEPUIS UN OBJECT DE COLLECTION
// LE MEME TRAVAIL QUE " MuseCeri "
public class PicterAdapter extends RecyclerView.Adapter<PicterAdapter.MyViewHolder> {

    List<String> pictuers;
    Context context;

    // CONSTRUCTEUR POUR RECUPERER LE CONTEXT ET LES IMAGES DEPUIS LE DETAIL_ACTIVITY ( voir ligne 59 MuseCeriDetailActivity )
    public PicterAdapter(Context context, List<String> pictuers) {
        this.context = context;
        this.pictuers = pictuers;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // STYLE D'AFFICHAGE DE L'IMAGE
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.onepictuer, parent, false);
      return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // AFFICHER L'IMAGE DEPUIS L'URL
        Glide.with(holder.itemView)
                .load(pictuers.get(position))
                .error(R.drawable.ic_baseline_broken_image_24)
                .fallback(R.drawable.ic_baseline_broken_image_24)
                .into(holder.imageObject);
    }

    @Override
    public int getItemCount() {
        return pictuers.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        // init the item view's
        ImageView imageObject;

        public MyViewHolder(View itemView) {
            super(itemView);
            // get the reference of item view's
            imageObject = (ImageView) itemView.findViewById(R.id.imageObject);
        }

    }
}
