package com.project.samirelidrissi;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    public static Retrofit getRetrofit(){

        //Appel au service API de CERIMUSEUM
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).build();

        // PREPARATION DE LA REQUETTE
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        return retrofit;
    }


    // EXECUTEUR DE REQUETTE depuis le Service
    public static MuseCeriService getMuseService(){
        MuseCeriService museCeriService = getRetrofit().create(MuseCeriService.class);
        return museCeriService;
    }

}
