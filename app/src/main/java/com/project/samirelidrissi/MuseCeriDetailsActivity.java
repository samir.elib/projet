package com.project.samirelidrissi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

// AFFICHER LE DETAIL D'UN OBJECT CLIQUER
public class MuseCeriDetailsActivity extends AppCompatActivity {

    //DECLARATION DES ELEMENTS XML
    TextView name,
            description,
            categories,
            timeFrame,
            year,
            brand,
            technicalDetails,
            working;
    ImageView imageObject;
    RecyclerView pictersview;

    // OBJET RECU DEPUIS LE MAINACTIVITY
    MuseCeriResponse museCeriResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_museceri_details);

        name = findViewById(R.id.name);
        description = findViewById(R.id.description);
        categories = findViewById(R.id.categories);
        timeFrame = findViewById(R.id.timeFrame);
        year = findViewById(R.id.year);
        brand = findViewById(R.id.brand);
        imageObject = findViewById(R.id.imageObject);
        pictersview = findViewById(R.id.pictersview);

        // RECUPERATION DE PARAMETTRE ( data ) DEPUIS LE MAINACTIVITY
        Intent intent = getIntent();
        if(intent.getExtras() !=null){

            //CONVERTIR LE PARAMETTRE EN TYPE  ( MuseCeriResponse )
            museCeriResponse = (MuseCeriResponse) intent.getSerializableExtra("data");

            // TESTER SI DES IMAGES EXISTS
            if(museCeriResponse.getPictures()!=null){
                // APPEL A L'ADAPTER POUR AFFICHER PLUSIEUR IMAGES DANS LE XML
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
                pictersview.setLayoutManager(linearLayoutManager);
                // call the constructor of CustomAdapter to send the reference and data to Adapter
                PicterAdapter customAdapter = new PicterAdapter(this.getApplicationContext(), museCeriResponse.getPictures());
                pictersview.setAdapter(customAdapter); // set the Adapter to RecyclerView
            }

            String nameData = museCeriResponse.getName();
            String descriptionData = museCeriResponse.getDescription();
            String categoriesData = museCeriResponse.getCategories();
            String timeFrameData = museCeriResponse.getTimeFrame();
            String yearData = museCeriResponse.getYear();
            String brandData = museCeriResponse.getBrand();

            // AFFICHER L'IMAGE DE L'OBJET EN COURS
            Glide.with(imageObject)
                    .load(museCeriResponse.imageObject())
                    .error(R.drawable.ic_baseline_broken_image_24)
                    .fallback(R.drawable.ic_baseline_broken_image_24)
                    .into(imageObject); //8

            name.setText(nameData);
            description.setText(descriptionData);
            categories.setText(categoriesData);
            timeFrame.setText(timeFrameData);
            year.setText(yearData);
            brand.setText(brandData);

        }


    }
}
